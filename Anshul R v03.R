#Following lines of code just set everything up
require(readr)
calls_rawdata = read_csv("InputData v02_1.csv",col_names = TRUE)

#Get relevant columns
calls_rawdata = calls_rawdata[,-c(2)] #take out Zipcode

#Create two datasets
grand_tot = seq(6,570,by=6) #take every 6th row
callsdata_total = calls_rawdata[grand_tot,] #now have the totals of each zipcode.
callsdata = calls_rawdata[-grand_tot,] #strictly the calls by CD and by Service Name

#Remove all missing cases from the dataset
cc1 = complete.cases(callsdata_total)
callsdata_total = callsdata_total[cc1,]

cc2 = complete.cases(callsdata)
callsdata = callsdata[cc2,]

callsdata_total$service <- factor(callsdata_total$service)
callsdata$service <- factor(callsdata$service)

require(ggplot2)
require(faraway)
require(car)
require(MASS)
cor(callsdata[,-c(2)])

#Removing highly correlated variables based on discussion
servicecalls = subset(callsdata,select = -c(pop,totalfams,marriedhouseholds,nonfamhouseholds,pop15andabove,avghousesize,agelessthan15,age15to24,medianage,sexratio,edcollegeorhigher,householdmeanincome,familymeanincome,householdmedianincome,familymedianincome,pop16inlaborforce,pop16unemp))
totalcalls = subset(callsdata_total,select = -c(pop,totalfams,marriedhouseholds,nonfamhouseholds,pop15andabove,avghousesize,agelessthan15,age15to24,medianage,sexratio,edcollegeorhigher,householdmeanincome,familymeanincome,householdmedianincome,familymedianincome,pop16inlaborforce,pop16unemp))                                            
                                            
totalcalls = as.data.frame(totalcalls[,-2])
servicecalls = as.data.frame(servicecalls)

summary(totalcalls)
cor(totalcalls[,-c(1,2)])

lmod1 <- lm(calls ~ .,data=totalcalls)
summary(lmod1)

lmod_service <- lm(calls~.,data=servicecalls)
summary(lmod_service)
x <- model.matrix(lmod1)[,-1]
vif(x)

#vif command not working so checking eigenvalues
e <- eigen(t(x) %*% x)
e$val

#calculate vif using for loop
vifvec <- 0

for(i in 1:ncol(x))
{
  vifvec[i] = 1/(1-summary(lm(x[,i] ~ x[,-i]))$r.squared)  
}
y = which(vifvec>10)
y
round(cor(x),2)

#the vif array shows that 3 variables have VIF>10 (avgfamsize, latino, edhsorhigher)
#looked at the correlation matrix - avgfamsize, latino,foreignborn,edhsorhigher and percapmean income are all highly correlated
#removing agedepratio and avgfamsize and totalhouseholds and latino

#idea: normalize calls w.r.t. population
#checked a lot of correlations and linear models.
#an analyst from chicago times analyzed their 311 calls data and used calls per capita as the response

servicecalls1 = servicecalls
servicecalls1$calls <- servicecalls1$calls/callsdata$pop

totalcalls1 = totalcalls
totalcalls1$calls <- totalcalls1$calls/callsdata_total$pop

#played around with a lot of predictors and these seem to bring out the best model - male,age60,married,foreignborn,percapmeanincome,pop16emp
lmod_total <- lm(calls ~ male + age60andabove + married + foreignborn + percapmeanincome +
                   pop16emp + agedepratio,data=totalcalls1)
summary(lmod_total)

plot(lmod_total$residuals ~ lmod_total$fitted.values)
abline(h=0)

#keeping multiple variables in the dataset and checking for collinearity
#once collinearity is taken care of, will check for outliers and leverages

totalcalls1$avghousesize <- callsdata_total$avghousesize
totalcalls1$medianage <- callsdata_total$medianage
lmod_total <- lm(calls ~ male + medianage+ married + foreignborn + percapmeanincome +
                   pop16emp + avghousesize,data=totalcalls1)
summary(lmod_total)

#seems like medianage is as good a predictor as age60andabove

#dropping some variables from totalcalls1
totalcalls2 <- subset(totalcalls1,select = -c(age25to39,age40to59,agedepratio,totalhouseholds,avgfamsize,latino))
lmod_total <- lm(calls ~ .,totalcalls2)
x <- model.matrix(lmod_total)[,-1]
#testing for collinearity
vifvec2 <- 0
for(i in 1:ncol(x))
{
  vifvec2[i] = 1/(1-summary(lm(x[,i] ~ x[,-i]))$r.squared)  
}
y2 = which(vifvec2>10)
y2

round(cor(x),2)
#age60, medianage, avghousesize, edhsorhigher have high VIF
#tried removing variaous combinations of the above variables, we have to remove medianage and edhs

totalcalls3 <- subset(totalcalls2,select = -c(medianage,edhsorhigher))
lmod_total <- lm(calls ~ .,totalcalls3)
x <- model.matrix(lmod_total)[,-1]
#testing for collinearity
vifvec3 <- 0
for(i in 1:ncol(x))
{
  vifvec3[i] = 1/(1-summary(lm(x[,i] ~ x[,-i]))$r.squared)  
}
y3 = which(vifvec3>10)
y3
vifvec3
summary(lmod_total)

#no collinearity in this matrix

#diagnostics

#constant variance
plot(fitted(lmod_total),residuals(lmod_total))
abline(h=0)
#seems constant

#normality
qqnorm(residuals(lmod_total))
qqline(residuals(lmod_total))

#leverages
qqnorm(rstandard(lmod_total))
abline(0,1)
#there is deviation

#outliers
stud <- rstudent(lmod_total)
stud[which.max(abs(stud))]
qt(0.05/(93*2),86)

#critical value is 3.59
stud[which(abs(stud)>3.59)]

# two values are above this critical value (486 and 504). will remove them and refit the model
match(c(210,504),rownames(totalcalls3))
#row number 79 and 82
lmod_total <- lm(calls ~ .,data=totalcalls3[-c(79,82),])

#constant variance
plot(fitted(lmod_total),residuals(lmod_total))
abline(h=0)
#variance improved. seems constants

#normality
qqnorm(residuals(lmod_total))
qqline(residuals(lmod_total))
#not normal. seems like cauchy distribution
#solution is to try some other distribution for model - glm

#leverages
qqnorm(rstandard(lmod_total))
abline(0,1)
#there is slight deviation. improvement from before

#outliers
stud <- rstudent(lmod_total)
stud[which.max(abs(stud))]
qt(0.05/(93*2),86)

#critical value is 3.59
stud[which(abs(stud)>3.59)]

#one more outlier, will have to remove it also

match(c(210),rownames(totalcalls3))
#row number 79 and 82 and 35
lmod_total <- lm(calls ~ .,data=totalcalls3[-c(35,79,82),])

#constant variance
plot(fitted(lmod_total),residuals(lmod_total))
abline(h=0)
#very good

#normality
qqnorm(residuals(lmod_total))
qqline(residuals(lmod_total))
#not normal. seems like cauchy distribution
#solution is to try some other distribution for model - glm

#leverages
qqnorm(rstandard(lmod_total))
abline(0,1)
#there is slight deviation. improvement from before

#outliers
stud <- rstudent(lmod_total)
stud[which.max(abs(stud))]
qt(0.05/(90*2),83)
#no more outliers

#correlated errors
acf(residuals(lmod_total))
#not correlated at all

#influential observations
cook <- cooks.distance(lmod_total)
halfnorm(cook,3)


#detecting non-linearity in predictors using termplots

termplot(lmod_total,partial.resid = TRUE)
#shows all plots one at a time, mostly linear relationship

#checking for transformations
boxcox(lmod_total,plotit = T)
boxcox(lmod_total,plotit = T, lambda = seq(-0.2,1,0.1))
#this shows that lambda lies in the range 0.02 to 0.78. we can try log, square root and cube root transformations

lmod_trans <- lm(I(calls^0.66) ~ .,data=totalcalls3[-c(35,79,82),])
summary(lmod_trans)
#R2 goes down if we transform, diagnostics dont improve significantly, many variables become insignificant

#Turns out if we remove all the 3 outliers then R2 drops by 15% and some predictors become insignificant
#so we will remove only 3 and refit, using model selection also
summary(lmod_total)
summary(lm(calls ~ .,data=totalcalls3[-c(79,82),-7]))

#using step function to find best predictors
lmod_step <- lm(calls ~ .,data=totalcalls3[-c(79,82),])

stripchart(data.frame(scale(totalcalls3)),method="jitter",las=2,vertical = TRUE)
#shows that, calls, male and percapmeanincome are skewed so we can use log transformation on these

step(lmod_step)
#step function shows that avghousesize needs to be removed

require(leaps)
b<-regsubsets(calls ~ male + married + foreignborn + percapmeanincome + pop16emp + medianage, totalcalls3[-c(79,82),])
rs <- summary(b)
rs$which[which.max(rs$adjr),]

#male is a significant predictor if we apply log or root transformation on response, we should apply log transformation on income, male, and married also
#no transformations also mean that male is a significant predictor

lmod_final <- lm(calls ~ male + married + foreignborn + log(percapmeanincome) + pop16emp + medianage, totalcalls3[-c(79,82),])
summary(lmod_final)

scatterplotMatrix(totalcalls3)

b<-regsubsets(calls^0.5 ~ male + married + foreignborn + log(percapmeanincome) + pop16emp + medianage, totalcalls3[-c(79,82),])
rs <- summary(b)
rs$which[which.max(rs$adjr),]
#constant variance
plot(fitted(lmod_final),residuals(lmod_final))
abline(h=0)
#very good

#normality
qqnorm(residuals(lmod_alternates))
qqline(residuals(lmod_alternates))
#not normal. seems like cauchy distribution
#solution is to try some other distribution for model - glm

#leverages
qqnorm(rstandard(lmod_alternates))
abline(0,1)
#there is slight deviation. improvement from before

#outliers
stud <- rstudent(lmod_final)
stud[which.max(abs(stud))]
qt(0.05/(91*2),85)
stud[which(abs(stud)>3.59)]

#one more outlier, will have to remove it also

match(c(216),rownames(totalcalls3))

#correlated errors
acf(residuals(lmod_final))
#not correlated at all

#influential observations
cook <- cooks.distance(lmod_total)
halfnorm(cook,3)


#percapmeanincome should be log transformed
ggplot(totalcalls3,aes(y=log(calls),x=log(medianage))) + geom_point()

lmod_try <- lm(calls ~ married + foreignborn + log(percapmeanincome) + pop16emp + log(male) + medianage , totalcalls3[-c(30,35,82),])
summary(lmod_try)

require(leaps)
step(lmod_alternates)
b<-regsubsets(calls ~ marriedhouseholds + foreignborn + percapmeanincome + pop16emp + sexratio + age60andabove, totalcalls4[-c(30,35,82),])
rs <- summary(b)
rs$which[which.max(rs$adjr2),]

#checking for transformations
boxcox(lmod_alternates,plotit = T)
boxcox(lmod_try,plotit = T, lambda = seq(-0.2,1,0.1))

plot(fitted(lmod_alternates),residuals(lmod_alternates))
abline(h=0)
qqnorm(residuals(lmod_alternates))
qqline(residuals(lmod_alternates))

stud <- rstudent(lmod_alternates)
stud[which.max(abs(stud))]
qt(0.05/(90*2),83)
stud[which(abs(stud)>3.59)]

acf(residuals(lmod_alternates))


#the best model in terms of diagnostics and reasonably good R2 ~ 50%
#x - log(male), medianage, pop16emp, log(percapmeanincome), foreignborn, married
#outliers - rows index - 30, 35, 82
#no transformation on calls per capita

#trying to increase R2
totalcalls3$totalhouseholds <- totalcalls1$totalhouseholds
totalcalls3$latino <- totalcalls1$latino

lmod_try <- lm(calls ~ married + foreignborn + log(percapmeanincome) + pop16emp + log(male) + medianage, totalcalls3[-c(30,35,82),])
summary(lmod_try)

termplot(lmod_try,partial.resid = TRUE)

servicecalls1$medianage <- callsdata$medianage
lmod_ser <- lm(calls ~ service + married + foreignborn + log(percapmeanincome) + pop16emp + log(male) + medianage,servicecalls1)
summary(lmod_ser)
#poor R2


x <- model.matrix(lmod_alternates)[,-1]
#testing for collinearity
vifvec5 <- 0
for(i in 1:ncol(x))
{
  vifvec5[i] = 1/(1-summary(lm(x[,i] ~ x[,-i]))$r.squared)  
}
y3 = which(vifvec5>10)
y3
vifvec5

#no collinearity


#trying alternative variables to improve fit
totalcalls4 = callsdata_total
totalcalls4$calls = totalcalls4$calls/totalcalls4$pop

require(faraway)
lmod_alternates <- lm(calls ~ marriedhouseholds + foreignborn + log(percapmeanincome) + pop16emp + sexratio + age60andabove, totalcalls4[-c(30,35,82),])
summary(lmod_alternates)

termplot(lmod_alternates, partial.resid=TRUE, col.res = "purple", smooth=panel.smooth)

#checked diagnostics
#no collinearity - highest VIF is 9.8
#constant variance assumption holds
#no correlation in errors
#normality of errors might not hold
#predictors are correctly transformed
#response might need transformation - boxcox shows 0.05 to 0.9

#checking for sensitivity of predictor coeffs
plot(dfbeta(lmod_alternates)[,4],ylab ="Change in coeff. of log(percapmeanincome)")
abline(h=0)

diags <- data.frame(lm.influence(lmod_alternates)$coef)
ggplot(diags,aes(row.names(diags),pop16emp)) +
  geom_linerange(aes(ymax=0,ymin=pop16emp)) +
  theme(axis.text.x=element_text(angle=90)) +
  xlab("row") +geom_hline(yint=0)


#Checking rmse
rmse <- function(x,y) sqrt(mean((x-y)^2))

#Test and train data
tempdata = totalcalls4[-c(30,35,82),]
ab <- seq(10, nrow(tempdata), by=10)
traindata <- tempdata[-ab,-2]
testdata <- tempdata[ab,-2]

traindata <- as.data.frame(traindata)
traindata <- as.data.frame(scale(traindata))
testdata <- as.data.frame(testdata)
testdata <- as.data.frame(scale(testdata))
lmod <- lm(calls ~ marriedhouseholds + foreignborn + percapmeanincome + pop16emp + sexratio, traindata)
summary(lmod)

rmse(fitted(lmod),traindata$calls)
rmse(predict(lmod,testdata),testdata$calls)
#super good rmse, 0.03 for train and 0.02 for test
#on scaled data, rmse is 0.68 and 0.61 (age60 becomes insignificant)

#Scaling helps in figuring out relative importance of the variables also

lmod_alternates <- lm(calls ~ marriedhouseholds + foreignborn + log(percapmeanincome) + pop16emp + age60andabove, totalcalls4[-c(2,30,35,82),])
sumary(lmod_alternates)
step(lmod_alternates)
summary(lmod_alternates)
par(mfrow = c(2,2))
plot(lmod_alternates)

stud <- rstudent(lmod_alternates)
stud[which.max(abs(stud))]
qt(0.05/(90*2),83)
#critical value is 3.59

stud[which(abs(stud)>3.59)]

#correlated errors
par(mfrow = c(1,1))
acf(residuals(lmod_alternates))

par(mfrow = c(1,1))
termplot(lmod_alternates, partial.resid=TRUE, col.res = "purple", smooth=panel.smooth)

par(mfrow = c(1,1))
#Sensitivity analysis
diags <- data.frame(lm.influence(lmod_alternates)$coef)
ggplot(diags,aes(row.names(diags),age60andabove)) +
  geom_linerange(aes(ymax=0,ymin=age60andabove)) +
  theme(axis.text.x=element_text(angle=90)) +
  xlab("row") +geom_hline(yint=0)


#checking for transformations on response
boxcox(lmod_alternates,plotit = T)
boxcox(lmod_alternates,plotit = T, lambda = seq(0.1,1,0.1))

#RMSE

#Checking rmse
rmse <- function(x,y) sqrt(mean((x-y)^2))

#Creating two datasets - Test and train data. Test data will be 10% of the original
tempdata = totalcalls4[-c(2,30,35,82),]
ab <- seq(10, nrow(tempdata), by=10)
traindata <- tempdata[-ab,-2]
testdata <- tempdata[ab,-2]

traindata <- as.data.frame(traindata)
traindata <- as.data.frame(scale(traindata))
testdata <- as.data.frame(testdata)
testdata <- as.data.frame(scale(testdata))
lmod <- lm(calls ~ marriedhouseholds + foreignborn + percapmeanincome + pop16emp + age60andabove, traindata)
summary(lmod)

rmse(fitted(lmod),traindata$calls)
rmse(predict(lmod,testdata),testdata$calls)

b<-regsubsets(calls ~ marriedhouseholds + foreignborn + log(percapmeanincome) + pop16emp + sexratio + age60andabove + agedepratio, totalcalls4[-c(30,35,82),])
rs <- summary(b)
rs$which[which.max(rs$adjr2),]

rownames(totalcalls4) <- seq(1,nrow(totalcalls4),by=1)


scatterplotMatrix(totalcalls4[-c(2,30,35,82),c("calls","marriedhouseholds","foreignborn","percapmeanincome","pop16emp","sexratio","age60andabove","agedepratio")])

summary(totalcalls4[-c(2,30,35,82),c("calls","marriedhouseholds","foreignborn","percapmeanincome","pop16emp","sexratio","age60andabove","agedepratio")])

