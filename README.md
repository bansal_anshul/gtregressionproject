# README #

This is a repo to share program code and output of a course project at Georgia Tech. (ISYE 6414: Regression Analysis).

The objective of the project was to model the effect of social-economic indicators on the annual volume of 311 service calls in Los Angeles, USA.
The repo contains the R code as well as the report and presentation with discussion of the final results. Unable to share raw data due to confidentiality agreement with the Mayor's office of Los Angeles.

All the analyses were done in R.

feel free to drop me an email at 'anshulbansal.iitkgp@gmail.com' if you would like to know more.